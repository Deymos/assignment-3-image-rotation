#define _POSIX_C_SOURCE 200112L
#include "bmp.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>



void bmp_to_image(FILE* bmp_input, struct image *new_image){
    struct bmp_header bmp_header_in;
    fread(&bmp_header_in, 1, sizeof(struct bmp_header), bmp_input);
    uint32_t padding_lenght = 0;
    *new_image = create_new_image(bmp_header_in.biWidth, bmp_header_in.biHeight);
	if((new_image->width*3)%4){
		padding_lenght = 4 - (new_image->width*3)%4;
	}

	if(sizeof(struct bmp_header) != bmp_header_in.bOffBits){
		fseek(bmp_input, bmp_header_in.bOffBits - (uint32_t)sizeof(struct bmp_header), SEEK_CUR);
	}

	new_image->pixel_table = (struct pixel*)malloc(new_image->width*new_image->height*3);

	uint32_t pixoffs = 0;

	for(uint32_t i = 0; i < new_image->height; i++){
		pixoffs += fread((char*)new_image->pixel_table+pixoffs, 1, new_image->width*3, bmp_input);
		if(padding_lenght){
			fseek(bmp_input, padding_lenght, SEEK_CUR);
		}
	}

	fseek(bmp_input, 0, SEEK_SET);


}


void image_to_bmp(const struct image img, FILE* bmp_output, FILE* bmp_input){
    struct bmp_header header;
    fseek(bmp_input, 0, SEEK_SET);
    fseek(bmp_output, 0, SEEK_SET);
    
    fread(&header, 1, sizeof(struct bmp_header), bmp_input);
    uint32_t padding_lenght = 4 - ((img.width*3)%4);
    header.biWidth = img.width;
    header.biHeight = img.height;
    header.bfileSize = img.width * img.height * sizeof(struct pixel) + sizeof(struct bmp_header) + padding_lenght * img.height;
    header.biSizeImage = img.height * (img.width + padding_lenght);

    fwrite(&header, sizeof (struct bmp_header), 1, bmp_output);

    int8_t byte = 0;
    

    for(int i = 0; i < header.biHeight; i++){
		fwrite(((char*)img.pixel_table)+(i*img.width*3), img.width*3, 1, bmp_output);
		for (int j = 0; j < padding_lenght; ++j) {
            fwrite(&byte, sizeof (int8_t), 1, bmp_output);        
        }
	}


}



