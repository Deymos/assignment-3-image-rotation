#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <stdint.h>

struct pixel
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
}__attribute__((packed));


struct image
{
    uint32_t width;
    uint32_t height;

    struct pixel* pixel_table;
    
};

struct image rotate_image_for_90_degrees(struct image old_image);
struct image create_new_image(uint32_t width, uint32_t height);
void image_clean(struct image *image);



#endif
