#include "image.h"
#include "bmp.h"
#include <stdio.h>


int main( int argc, char** argv ) {

    if(argc != 3){
		return 1;
	}


    FILE* read_file = fopen(argv[1], "rb");
    FILE* write_file = fopen(argv[2], "wb");

    if(read_file == NULL){
        fprintf(stderr, "Input file not found.");
        return 10;
    }
    if(write_file == NULL){
        fprintf(stderr, "Output file not found.");
        fclose(read_file);
        return 11;
    }



    struct image new_image;
    bmp_to_image(read_file, &new_image);

    printf("%ld %ld %ld\n", ((uint64_t)new_image.pixel_table),((uint64_t)(((void*)new_image.pixel_table))+8),((uint64_t)(((void*)new_image.pixel_table))+16));
    printf("\n");
    struct image rotated_image = rotate_image_for_90_degrees(new_image);

    printf("%ld %ld %ld\n", ((uint64_t)rotated_image.pixel_table),((uint64_t)(((void*)rotated_image.pixel_table))+8),((uint64_t)(((void*)rotated_image.pixel_table))+16));

    image_to_bmp(rotated_image, write_file, read_file);

    image_clean(&new_image);
    image_clean(&rotated_image);

    fclose(read_file);
    fclose(write_file);
    return 0;

}


