#include "../include/image.h"
#include "../include/bmp.h"
#include <stdlib.h>


int main( int argc, char** argv ) {

    if(argc != 3){
		return 1;
	}


    FILE* read_file = fopen(argv[1], "rb");
    FILE* write_file = fopen(argv[2], "wb");

    if(read_file == NULL){
        fprintf(stderr, "Get error while input read file");
        return 10;
    }
    if(write_file == NULL){
        fprintf(stderr, "Get error while output read file");
        fclose(read_file);
        return 11;
    }



    struct image* new_image;
    new_image = bmp_to_image(read_file);

    
    struct image rotated_image = rotate_image_for_90_degrees(new_image);

    
    image_to_bmp(rotated_image, write_file, read_file);

    free(new_image->pixel_table);
    free(rotated_image.pixel_table);
    free(new_image);
    // free(&rotated_image);

    fclose(read_file);
    fclose(write_file);
    return 0;

}
