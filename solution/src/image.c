#include "../include/image.h"
#include <stdlib.h>
#include <string.h>

struct image rotate_image_for_90_degrees(struct image* old_image){
    uint32_t width = old_image->width;
    uint32_t height  = old_image->height;
    struct image rotated_image = create_new_image(height, width);
    for(int i = 0; i < height; i++){
        for(int j  = 0; j < width; j++){
            rotated_image.pixel_table[j * height + (height - 1 - i)] = old_image->pixel_table[i*width+j];
        }
    }
    return rotated_image;
}

struct image create_new_image(uint32_t width, uint32_t height){
    return (struct image){
        .width = width,
        .height = height,
        .pixel_table = malloc(sizeof(struct pixel) * width * height)
    };
}




